# feed-item

A component that displays title text and a set of images

## Features

 - Displays a header title and a set of images

## Usage

```javascript
import FeedItem from './feed-item/main'
```
```javascript
<FeedItem title={title} images={images} />
```

### Properties

|Property Name|Type|Description|
|-|-|-|
|title|String|Displays text as the title|
|images|Array(String)|Displays images from image URLs|

### Components

Components required by the module

---

#### image-main

Displays the main image

|Property Name|Type|Description|
|-|-|-|
|source|String|Image source URL|

---

#### image-minor

Displays a list of minor images

|Property Name|Type|Description|
|-|-|-|
|sources|Array(String)|List of image source URLs|

---

## Authors

*James Nicholls*
