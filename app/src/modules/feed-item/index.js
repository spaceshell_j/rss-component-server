import React from'react'
import ImageMain from'./image-main'
import ImageMinor from'./image-minor'

const FeedItem = ({ title, links }) => {
	const [hero, ...images] = links
	return !!title ? (
		<div className="">
			<h2 className="P(16px) Bgc(#eeeeee) BdB">{title}</h2>
			<div className="D(f) Fld(r) H(250px) P(16px)">
				<ImageMain source={hero}/>
				<ImageMinor sources={images}/>
			</div>
		</div>
	) : (
		<h1>No Feed Item!</h1>
	)
}

export default FeedItem;
