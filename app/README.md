# RSS Component Server - Client

A client side application to implement the RSS component Server AWS Lambda function.

## Table of Contents
1. [Requirements](#requirements)
1. [Installation](#getting-started)
1. [Running the Project](#running-the-project)
1. [Project Structure](#project-structure)
1. [Testing](#testing)
1. [Deployment](#deployment)

## Requirements
* Node `^5.0.0`
* NPM `^3.0.0`


## Installation

Install all package dependencies

```bash
npm install
```


## Running the Project

To run the project simply build the application the using the command below and the project will be built and packaged into the `build` folder.

```bash
npm run build
```

>The API endpoint URL is located in the `index.js` file in the `src` folder

The following list of development commands are available for this projects.

|Command|Description|
|-|-|
|start|Starts the Webpack development server|
|test|Runs Jest's unit testing on any `.test.js` files|
|build|Minifies, lints and bundles any working files for production|
|eject|Breaks down the react-scripts package into its components|
|build:atomic|Builds the Atomic CSS file|
|watch:atomic|Builds the Atomic CSS file autonomously on file change|

## Modules and Components

Here is an overview of the modules and components available within the project.

|Components|Description|
|-|-|
|Feed Item|Displays a given text title and image array|
|Feed Item > Image Main|Renders a given Hero image from source URL|
|Feed Item > Image Minor|Renders an array of images from source URL's|

## Testing
To add a unit test, create a `.test.js` file inside of your `page`, `module` or `component` folder. Jest will automatically find these files and test them using the `npm test` command or via pipeline commands.
