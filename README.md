# RSS Component Server

This package contains a both a client and server application to retrieve, parse and render an RSS feed. The server component utilizes the AWS environment and the Serverless framework. The client side application is built with ReactJS and packaged for deployment using WebPack.

## Table of Contents
1. [Requirements](#requirements)
1. [Installation](#getting-started)
1. [Running the Project](#running-the-project)

## Requirements
* Node `^5.0.0`
* NPM `^3.0.0`

## Installation

After confirming that your environment meets the above [requirements](#requirements), you can create a new project by
doing the following:

```bash
$ git clone https://gitlab.com/spaceshell_j/rss-component-server.git <my-project-name>
$ cd <my-project-name>
```

To install all dependent packages to both the server and the client application simply run the command `npm run init`

```bash
$ npm run init
```

## Running the Project

Please refer to the individual README.md files for the corresponding `app` and `server` folders for an explanation on how to use the repo.

## Author

*James Nicholls*
